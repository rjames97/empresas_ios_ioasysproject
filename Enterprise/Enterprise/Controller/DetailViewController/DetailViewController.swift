//
//  DetailViewController.swift
//  Enterprise
//
//  Created by Robson James Junior on 21/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit
import Stevia

class DetailViewController: UIViewController, ArchitectureStandard {
  
  typealias textViewStyle = DetailStyleKit.DescriptionTextView
  typealias imageViewStyle = DetailStyleKit.EnterpriseImageView
  typealias mainSK = DetailStyleKit.MainScreen
  
  public var detailEnterprise: Enterprise? = nil
  private let enterpriseImageView = UIImageView()
  private let descriptionTextView = UITextView()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    commonInit()
  }
  
  func style() {
    self.view.backgroundColor = mainSK.backgroundColor
    navigationController?.navigationBar.tintColor = mainSK.navigationColor
    navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    
    enterpriseImageView.backgroundColor = .darkGray //TODO: The API does not work with the image, it changes when the API is updated
    enterpriseImageView.cropView(withValue: imageViewStyle.Sizes.borderValue)
    setTextFieldStyle()
    if let enterprise =  detailEnterprise  {
      self.title = enterprise.name
      descriptionTextView.text = enterprise.description
    }
  }
  
  func subview() {
    view.sv(enterpriseImageView)
    view.sv(descriptionTextView)
  }
  
  func layout() {
    enterpriseImageView.Top == view.safeAreaLayoutGuide.Top + imageViewStyle.Sizes.topMargin
    enterpriseImageView.Left == view.safeAreaLayoutGuide.Left + imageViewStyle.Sizes.lateralsMargin
    enterpriseImageView.Right == view.safeAreaLayoutGuide.Right - imageViewStyle.Sizes.lateralsMargin
    enterpriseImageView.Height == imageViewStyle.Sizes.height
    
    descriptionTextView.Top == enterpriseImageView.Bottom + textViewStyle.Sizes.generalMargin
    descriptionTextView.Bottom == view.safeAreaLayoutGuide.Bottom - textViewStyle.Sizes.generalMargin
    descriptionTextView.Left == view.safeAreaLayoutGuide.Left + textViewStyle.Sizes.generalMargin
    descriptionTextView.Right == view.safeAreaLayoutGuide.Right - textViewStyle.Sizes.generalMargin
  }
  
  private func setTextFieldStyle() {
    descriptionTextView.backgroundColor = mainSK.backgroundColor
    descriptionTextView.font = textViewStyle.Fonts.description
    descriptionTextView.textColor = textViewStyle.Colors.textColor
    descriptionTextView.isEditable = false
    descriptionTextView.isUserInteractionEnabled = true
    descriptionTextView.isScrollEnabled = true
  }
}
