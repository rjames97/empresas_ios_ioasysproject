//
//  ViewController.swift
//  Enterprise
//
//  Created by Robson James Junior on 18/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit
import Stevia

class LoginViewController: UIViewController, ArchitectureStandard {
  
  private let maxcharacterleght: Int = 30
  private let mincharacterleght: Int = 8
  
  internal var keyboardState: KeyboardState = .hiddenKeyboard
  private let cloudManager = CloudManager.shared
  
  private typealias buttonComponent = LoginSK.LoginButton
  private typealias bannerComponent = LoginSK.LogoBanner
  private typealias passwordTextFieldComponent = LoginSK.PasswordTextField
  private typealias emailTextFieldComponent = LoginSK.EmailTextField
  private typealias labelsComponent = LoginSK.StackViewLabels.Labels
  private typealias mainScreenSK = LoginSK.MainScreen
  
  private let labelsStackView = UIStackView()
  private let welcomeTitleLabel = UILabel()
  private let welcomeSubTitleLabel = UILabel()
  
  private let emailTextField = UITextField()
  private let passWordTextField = UITextField()
  private let bannerImage = UIImageView()
  private let logInButton = UIButton(type: .system)
  
  private let spinner = SpinnerViewController.shared
  
  override func viewDidLoad() {
    super.viewDidLoad()
    commonInit()
    addObserver()
    passWordTextField.delegate = self
    emailTextField.delegate = self
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  private func addObserver() {
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillShow),
                                           name: UIResponder.keyboardWillShowNotification,
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillHide),
                                           name: UIResponder.keyboardWillHideNotification,
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(signAuthorized),
                                           name: .signSucess,
                                           object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(errorInAuthentication),
                                           name: .signFail,
                                           object: nil)
  }
  
  func subview() {
    view.sv(bannerImage)
    labelsStackView.addArrangedSubview(welcomeTitleLabel)
    labelsStackView.addArrangedSubview(welcomeSubTitleLabel)
    view.sv(labelsStackView)
    view.sv(emailTextField)
    view.sv(passWordTextField)
    view.sv(logInButton)
  }
  
  func layout() {
    bannerLayout()
    stackViewLayout()
    textFieldsLayout()
    buttonsLayout()
  }
  
  func style() {
    view.backgroundColor = mainScreenSK.Colors.backGroundColor
    textFieldsStyle()
    buttonsStyle()
    setBanner()
    labelsStyle()
  }
  
  //MARK: Login Button Handle
  @objc func handleLogInRequest() {
    emailTextField.resignFirstResponder()
    passWordTextField.resignFirstResponder()
    
    guard let email = emailTextField.text, let password = passWordTextField.text else {
      return
    }
    if checkInputData(email: email, password: password) {
      let user = User(email: email, password: password)
      cloudManager.sign(user: user)
      createSpinnerView()
    } else {
      return
    }
  }
  
  //MARK: Notificiations Actions Selector
  @objc func signAuthorized() {
    DispatchQueue.main.async {
      self.destroySpinner()
      let enterprisesVC = EnterprisesViewController()
      let enterpriseNavigationControler = UINavigationController(rootViewController: enterprisesVC)
      enterpriseNavigationControler.modalPresentationStyle = .fullScreen
      self.present(enterpriseNavigationControler, animated: false, completion: nil)
    }
  }
  
  @objc func errorInAuthentication() {
    DispatchQueue.main.async {
      self.destroySpinner()
      DispatchQueue.main.async {
        self.showAlert(title: "Erro na Autenticação!",
                       message: "",
                       buttonTitle: "Ok",
                       buttonHandle: nil,
                       completion: nil)
      }
    }
  }
  
  //MARK: Spinner Actions
  private func createSpinnerView() {
    self.spinner.createSpinnerView(vController: self)
  }
  
  private func destroySpinner () {
    self.spinner.destroySpinner()
  }
}

//MARK: Set Element Style
extension LoginViewController {
  private func textFieldsStyle() {
    setEmailTextFieldStyle()
    setPasswordTextFieldStyle()
  }
  
  private func setEmailTextFieldStyle() {
    emailTextField.textAlignment = .left
    emailTextField.backgroundColor = emailTextFieldComponent.Colors.textColor
    emailTextField.clipsToBounds = true
    emailTextField.layer.cornerRadius = emailTextFieldComponent.Sizes.cornerRadiusValue
    emailTextField.layer.borderWidth = emailTextFieldComponent.Sizes.borderWidthValue
    emailTextField.layer.borderColor = emailTextFieldComponent.Colors.borderColor
    emailTextField.textColor = emailTextFieldComponent.Colors.subtitles
    emailTextField.attributedPlaceholder =
      NSAttributedString(string: emailTextFieldComponent.Texts.placeHolder,
                         attributes: [NSAttributedString.Key.foregroundColor :
                          emailTextFieldComponent.Colors.subtitles])
    setTextFieldIcon(textField: emailTextField,
                     imageName: emailTextFieldComponent.Texts.iconName,
                     height: 20, width: 15)
  }
  
  private func setPasswordTextFieldStyle() {
    passWordTextField.backgroundColor = passwordTextFieldComponent.Colors.textColor
    passWordTextField.clipsToBounds = true
    passWordTextField.layer.cornerRadius = passwordTextFieldComponent.Sizes.cornerRadiusValue
    passWordTextField.layer.borderColor = passwordTextFieldComponent.Colors.borderColor
    passWordTextField.layer.borderWidth = passwordTextFieldComponent.Sizes.borderWidthValue
    passWordTextField.textAlignment = .left
    passWordTextField.isSecureTextEntry = true
    passWordTextField.textColor = passwordTextFieldComponent.Colors.subtitles
    passWordTextField.attributedPlaceholder =
      NSAttributedString(string: passwordTextFieldComponent.Texts.placeHolder,
                         attributes: [NSAttributedString.Key.foregroundColor :
                          passwordTextFieldComponent.Colors.subtitles])
    setTextFieldIcon(textField: passWordTextField,
                     imageName: passwordTextFieldComponent.Texts.iconName,
                     height: 15, width: 22)
  }
  
  private func setTextFieldIcon(textField: UITextField, imageName: String, height: CGFloat, width: CGFloat) {
    guard let iconImage = getImage(imageName: imageName) else {
      return
    }
    textField.setIcon(iconImage, height: height, width: width)
  }
  
  private func setBanner() {
    guard let image = getImage(imageName: bannerComponent.Texts.imageName) else {
      return
    }
    bannerImage.image = image
  }
  
  private func buttonsStyle() {
    logInButton.backgroundColor = buttonComponent.Colors.backGroundColor
    logInButton.setTitle(buttonComponent.Texts.title, for: .normal)
    logInButton.setTitleColor(buttonComponent.Colors.textColor, for: .normal)
    logInButton.clipsToBounds = true
    logInButton.layer.cornerRadius = buttonComponent.Sizes.cornerRadiusValue
    logInButton.titleLabel?.font = buttonComponent.Font.title
    logInButton.addTarget(self,
                          action: #selector(handleLogInRequest),
                          for: .touchUpInside)
  }
  
  private func labelsStyle() {
    welcomeTitleLabel.text = labelsComponent.TitleProperty.text
    welcomeSubTitleLabel.text = labelsComponent.SubTitleProperty.text
    welcomeTitleLabel.numberOfLines = 0
    welcomeSubTitleLabel.numberOfLines = 0
    welcomeTitleLabel.textAlignment = .center
    welcomeSubTitleLabel.textAlignment = .center
    
    welcomeTitleLabel.font = labelsComponent.TitleProperty.font
    welcomeSubTitleLabel.font = labelsComponent.SubTitleProperty.font
  }
}

//MARK: Set Elements Layout
extension LoginViewController {
  private func bannerLayout() {
    bannerImage.centerHorizontally()
    bannerImage.Top == view.safeAreaLayoutGuide.Top + bannerComponent.Sizes.marginToTop
    bannerImage.width(bannerComponent.Sizes.widthValue)
    bannerImage.height(bannerComponent.Sizes.heightValue)
  }
  
  private func stackViewLayout() {
    labelsStackView.Top == bannerImage.Bottom + 44
    labelsStackView.centerHorizontally().fillHorizontally(m: 42)
    labelsStackView.axis = .vertical
    labelsStackView.spacing = 16
  }
  
  private func buttonsLayout() {
    emailTextField.centerHorizontally()
    emailTextField.fillHorizontally(m: emailTextFieldComponent.Sizes.margin)
    emailTextField.Height == emailTextFieldComponent.Sizes.heightValue
    emailTextField.Top == labelsStackView.Bottom + emailTextFieldComponent.Sizes.marginToTop
    
    passWordTextField.centerHorizontally()
    passWordTextField.fillHorizontally(m: passwordTextFieldComponent.Sizes.margin)
    passWordTextField.Height == passwordTextFieldComponent.Sizes.heightValue
    passWordTextField.Top == emailTextField.Bottom + passwordTextFieldComponent.Sizes.marginToBottom
  }
  
  private func textFieldsLayout() {
    logInButton.centerHorizontally()
    logInButton.Top == passWordTextField.Bottom + buttonComponent.Sizes.spacingToTop
    logInButton.fillHorizontally(m: buttonComponent.Sizes.margin)
    logInButton.Height == buttonComponent.Sizes.heightValue
  }
}

//MARK: UITextFieldDelegate and KeyBoard
extension LoginViewController: UITextFieldDelegate {
  
  internal enum KeyboardState {
    case showKeyboard
    case hiddenKeyboard
  }
  
  @objc func keyboardWillShow(notification: NSNotification) {
    if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey]
      as? NSValue)?.cgRectValue {
      if keyboardState == .hiddenKeyboard {
        self.view.frame.origin.y -= keyboardSize.height / 2
        keyboardState = .showKeyboard
      }
    }
  }
  
  @objc func keyboardWillHide(notification: NSNotification) {
    if keyboardState == .showKeyboard {
      self.view.frame.origin.y = 0
      keyboardState = .hiddenKeyboard
    }
  }
  
  func textFieldShouldReturn(userText: UITextField!) -> Bool {
    userText.resignFirstResponder()
    return true;
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    passWordTextField.resignFirstResponder()
    emailTextField.resignFirstResponder()
    return false
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    passWordTextField.resignFirstResponder()
    emailTextField.resignFirstResponder()
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    let defaultColor = passwordTextFieldComponent.Colors.subtitles
    if textField.textColor != defaultColor {
      textField.textColor = defaultColor
    }
  }
}

//MARK: Check Input Data
extension LoginViewController {
  internal func checkInputData(email: String, password: String) -> Bool {
    if email == "", email.isValidEmail(), email.contains(" ") {
      incorrectCredential()
      emailIncorrect()
      return false
    }
    if email.count > self.maxcharacterleght || email.count < self.mincharacterleght {
      emailIncorrect()
      incorrectCredential()
      return false
    }
    if password == "" || password.contains(" ") {
      incorrectCredential()
      passwordIncorrect()
      return false
    }
    if password.count > self.maxcharacterleght || password.count < self.mincharacterleght {
      passwordIncorrect()
      incorrectCredential()
      return false
    }
    return true
  }
  
  private func incorrectCredential() {
    DispatchQueue.main.async {
      self.showAlert(title: "Credenciais informadas são inválidas, tente novamente.",
                     message: "",
                     buttonTitle: "Ok",
                     buttonHandle: nil,
                     completion: nil)
    }
  }
  
  private func passwordIncorrect() {
    self.passWordTextField.textColor = .red
  }
  
  private func emailIncorrect() {
    self.emailTextField.textColor = .red
  }
}
