//
//  EnterprisesSearchDataSource.swift
//  Enterprise
//
//  Created by Robson James Junior on 23/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit

class EnterpriseSearchBarDataSource: NSObject, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return EnterprisesViewController.searchedEnterprise.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: EnterprisesTableViewCell.identifier,
                                                   for: indexPath) as? EnterprisesTableViewCell
      else {
        return UITableViewCell()
    }
    let enterpriseSearch = EnterprisesViewController.searchedEnterprise[indexPath.row]
    cell.enterpriseNameLabel.text = enterpriseSearch.name
    cell.enterpriseTypeNameLabel.text = enterpriseSearch.enterpriseType.category
    cell.enterpriseCountryLabel.text = enterpriseSearch.country
    return cell
  }
}
