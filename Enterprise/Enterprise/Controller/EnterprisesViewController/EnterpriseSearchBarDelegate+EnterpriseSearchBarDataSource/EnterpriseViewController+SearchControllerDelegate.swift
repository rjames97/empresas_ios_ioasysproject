//
//  EnterpriseViewController+SearchControllerDelegate.swift
//  Enterprise
//
//  Created by Robson James Junior on 23/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit

enum SearchState {
  case inSearch
  case nonSearch
}

enum EmptyStateSearch {
  case results
  case nonResults
  case allHidden
}

extension EnterprisesViewController: UISearchBarDelegate {
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchBar.text = nil
    searchState = .nonSearch
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    searchState = .nonSearch
  }
  
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    guard let searchText = searchBar.text else {
      return
    }
    dataLoaded = true
    searchState = .inSearch
    modifySearchBarState(isActive: dataLoaded)
    cloudManager.searchEnterprise(with: searchText)
  }
  
  internal func transitionToStateEmpty(to state: EmptyStateSearch) {
    if state == .nonResults {
      DispatchQueue.main.async {
        UIView.animate(withDuration: 0.4, animations: {
          self.emptyStateView.isHidden = false
        })
      }
    } else if state == .results {
      DispatchQueue.main.async {
        UIView.animate(withDuration: 0.4, animations: {
          self.emptyStateView.isHidden = true
        })
      }
    } else {
      DispatchQueue.main.async {
        UIView.animate(withDuration: 0.4, animations: {
          self.emptyStateView.isHidden = true
        })
      }
    }
  }
  
  internal func transitionSearchingToState(to state: SearchState) {
    if state == .inSearch {
      DispatchQueue.main.async {
        UIView.animate(withDuration: 0.4, animations: {
          self.tableView.isHidden = true
          self.tableViewSearch.isHidden = false
          self.tableViewSearch.reloadData()
        })
      }
    } else if state == .nonSearch {
      DispatchQueue.main.async {
        UIView.animate(withDuration: 0.4, animations: {
          self.tableView.isHidden = false
          self.tableViewSearch.isHidden = true
          self.emptyStateSearchBar = .allHidden
          self.tableView.reloadData()
        })
      }
    }
  }
  
  func modifySearchBarState(isActive: Bool) {
    DispatchQueue.main.async { self.navigationItem.searchController?.searchBar.isUserInteractionEnabled = isActive
    }
  }
  
  func modifyTableViewState(isEnabled: Bool) {
    DispatchQueue.main.async {
      self.tableView.allowsSelection = isEnabled
    }
  }
  
  @objc
  private func reloadTableView() {
    DispatchQueue.main.async {
      self.dataLoaded = true
      self.modifySearchBarState(isActive: self.dataLoaded)
      self.modifyTableViewState(isEnabled: self.dataLoaded)
      self.tableView.reloadData()
    }
  }
}
