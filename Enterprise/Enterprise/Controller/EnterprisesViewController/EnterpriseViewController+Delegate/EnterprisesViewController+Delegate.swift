//
//  EnterprisesViewController+Delegate.swift
//  Enterprise
//
//  Created by Robson James Junior on 22/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit

extension EnterprisesViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    var currentEnterprise: Enterprise? = nil
    if searchState == .inSearch {
      currentEnterprise = EnterprisesViewController.searchedEnterprise[indexPath.row]
    } else {
      currentEnterprise = enterprisesManager?[indexPath.row]
    }
    let detailVC = DetailViewController()
    detailVC.detailEnterprise = currentEnterprise
    navigationController?.pushViewController(detailVC, animated: true)
    tableView.deselectRow(at: indexPath, animated: true)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return mainSK.Sizes.heightForRowAt 
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 0 
  }
}
