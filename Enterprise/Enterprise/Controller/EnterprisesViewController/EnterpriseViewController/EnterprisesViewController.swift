//
//  EnterprisesViewController.swift
//  Enterprise
//
//  Created by Robson James Junior on 21/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit
import Stevia

class EnterprisesViewController: UIViewController, ArchitectureStandard {
  
  internal var enterprisesManager: [Enterprise]? = nil
  internal let cloudManager = CloudManager.shared
  private let spinner = SpinnerViewController.shared
  
  typealias mainSK = EnterpriseStyleKit.MainScreen
  typealias searchApparence = EnterpriseStyleKit.SearchApparence
  typealias navigationApparence = EnterpriseStyleKit.NavigationApparence
  
  internal let tableView = UITableView(frame: .zero, style: .grouped)
  internal let logoImageView = UIImageView()
  
  internal let searchEnterprises = UISearchController()
  internal let tableViewSearch = UITableView(frame: .zero, style: .grouped)
  internal var searchDataSource = EnterpriseSearchBarDataSource()
  static var searchedEnterprise = [Enterprise]()
  internal var emptyStateView = EmptyStateView()
  internal var dataLoaded = false
  
  internal var searchState: SearchState = .nonSearch {
    didSet {
      if searchState == .inSearch {
        transitionSearchingToState(to: .inSearch)
        createSpinnerView()
      } else if searchState == .nonSearch {
        transitionSearchingToState(to: .nonSearch)
        destroySpinner()
      }
    }
  }
  
  public var emptyStateSearchBar: EmptyStateSearch = .allHidden {
    didSet {
      if emptyStateSearchBar == .nonResults {
        transitionToStateEmpty(to: .nonResults)
      } else if emptyStateSearchBar == .results {
        transitionToStateEmpty(to: .results)
      } else if emptyStateSearchBar == .allHidden {
        transitionToStateEmpty(to: .allHidden)
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    modifySearchBarState(isActive: dataLoaded)
    modifyTableViewState(isEnabled: dataLoaded)
    cloudManager.showAllEnterprises()
    addObserver()
    commonInit()
    createSpinnerView()
    tableView.delegate = self
    tableView.dataSource = self
    tableViewSearch.delegate = self
    tableViewSearch.dataSource = searchDataSource
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  private func addObserver() {
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(enterprisesDataReceive(notification:)),
                                           name: .enterprisesSucess,
                                           object: nil)
    
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(errorReceived(notification:)),
                                           name: .enterprisesFail,
                                           object: nil)
    
    NotificationCenter.default.addObserver(self,
                                              selector: #selector(searchDataReceive(notification:)),
                                              name: .searchedEnterprises,
                                              object: nil)
  }
  
  func style() {
    navigationControllerApparence()
    tablesViewStyle()
    title = " "
  }
  
  private func navigationControllerApparence() {
    searchEnterprises.searchBar.tintColor = searchApparence.Colors.tintColor
    searchEnterprises.searchBar.searchTextField.backgroundColor = searchApparence.Colors.barBackgroundColor
    
    navigationController?.navigationBar.barTintColor = navigationApparence.Colors.backgroundColor
    navigationItem.searchController = searchEnterprises
    logoImageView.image = UIImage(named: navigationApparence.Texts.logoName)
    navigationItem.titleView = logoImageView
    navigationItem.searchController?.searchBar.placeholder = searchApparence.Texts.placeHolderText
    navigationItem.hidesSearchBarWhenScrolling = false
    navigationItem.searchController?.searchBar.delegate = self
    emptyStateView.isHidden = true
    
    let logoutBarButton =  UIBarButtonItem(image: nil,
                                         style: .plain,
                                         target: self,
                                         action: #selector(logoutEnterprises))
    logoutBarButton.tintColor = .white
  logoutBarButton.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18, weight: .bold)], for: .normal)
    
    navigationItem.rightBarButtonItem = logoutBarButton
    navigationItem.rightBarButtonItem?.title = "Sair"
  }
  
  private func tablesViewStyle() {
    tableView.backgroundColor = mainSK.Colors.backGroundColor
    tableView.register(EnterprisesTableViewCell.self,
                       forCellReuseIdentifier: EnterprisesTableViewCell.identifier)
    
    
    tableViewSearch.backgroundColor = mainSK.Colors.backGroundColor
    tableViewSearch.register(EnterprisesTableViewCell.self,
                             forCellReuseIdentifier: EnterprisesTableViewCell.identifier)
    
    tableViewSearch.separatorStyle = .none
    tableViewSearch.isHidden = true
    
    tableView.separatorStyle = .none
    tableViewSearch.separatorStyle = .none
  }
  
  func subview() {
    view.sv(tableViewSearch)
    view.sv(tableView)
    view.sv(emptyStateView)
  }
  
  func layout() {
    tableView.fillContainer()
    emptyStateView.centerInContainer()
    emptyStateView.size(EmptyViewStyle.Sizes.heigth)
    
    logoImageView.height(navigationApparence.Sizes.heightBanner)
    logoImageView.width(navigationApparence.Sizes.widthBanner)
    
    tableViewSearch.fillContainer()
    tableViewSearch.insetsContentViewsToSafeArea = false
  }
  
  //MARK: Spinner Actions
  internal func createSpinnerView() {
    self.spinner.createSpinnerView(vController: self)
  }
  
  internal func destroySpinner () {
    self.spinner.destroySpinner()
  }
  
  @objc func searchDataReceive(notification: Notification) {
    destroySpinner()
    if let enterprises = notification.userInfo?["searchedEnterprises"] as? [Enterprise] {
    EnterprisesViewController.searchedEnterprise.removeAll()
      enterprises.forEach { (enterprises) in
        EnterprisesViewController.searchedEnterprise.append(enterprises)
      }
      if EnterprisesViewController.searchedEnterprise.isEmpty {
        self.emptyStateSearchBar = .nonResults
      } else {
        self.emptyStateSearchBar = .results
      }
      DispatchQueue.main.async {
        self.tableViewSearch.reloadData()
      }
    }
  }
  
  @objc func enterprisesDataReceive(notification: Notification) {
    destroySpinner()
    if let enterprises = notification.userInfo?["enterprises"] as? [Enterprise] {
      self.enterprisesManager = enterprises
      dataLoaded = true
      modifySearchBarState(isActive: dataLoaded)
      modifyTableViewState(isEnabled: dataLoaded)
      DispatchQueue.main.async {
        self.tableView.reloadData()
      }
    }
  }
  
  @objc private func errorReceived(notification: Notification) {
    var messageString: String = ""
    if let message = notification.userInfo?["errorMessage"] as? [String] {
      messageString = message.first ?? "Faça Login novamente!"
    }
    
    DispatchQueue.main.async {
      self.showAlert(title: "Erro de conexão com servidor!",
                     message: messageString,
                     buttonTitle: "Fazer Login",
                     buttonHandle: { (UIAlertAction) in
                      self.logoutHandle()
      },
                     completion: nil)
    }
  }
  
  @objc private func logoutEnterprises() {
    DispatchQueue.main.async {
    self.showAlert(title: "Você deseja sair?",
                   message: "Se sair, deverá fazer login novamente!",
                   buttonTitle: "Sair",
                   style: .actionSheet,
                   buttonHandle: { (UIAlertAction) in
                    self.logoutHandle()
                   },
                   buttonStyle: .destructive,
                   cancelButtonTitle: "Cancelar",
                   cancelButtonHandle: nil,
                   cancelStyle: .cancel,
                   completion: nil)
    }
  }
  
  private func logoutHandle() {
    let loginVC = LoginViewController()
    loginVC.modalPresentationStyle = .fullScreen
    self.present(loginVC, animated: true) { () in
      CloudManager.destroy()
    }
  }
}
