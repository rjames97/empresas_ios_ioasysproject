//
//  EnterprisesViewController+DataSource.swift
//  Enterprise
//
//  Created by Robson James Junior on 22/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit

extension EnterprisesViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return enterprisesManager?.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: EnterprisesTableViewCell.identifier,
                                                   for: indexPath) as? EnterprisesTableViewCell
      else { return UITableViewCell() }
    guard let enterprise = enterprisesManager?[indexPath.row] else {
      return UITableViewCell()
    }
    cell.enterpriseNameLabel.text = enterprise.name
    cell.enterpriseTypeNameLabel.text = enterprise.enterpriseType.category
    cell.enterpriseCountryLabel.text = enterprise.country
    return cell
  }
}
