//
//  DetaiStyleKit.swift
//  Enterprise
//
//  Created by Robson James Junior on 24/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit

struct DetailStyleKit {
  struct MainScreen {
    static let backgroundColor = UIColor(red: 235, green: 233, blue: 215)
    static let navigationColor: UIColor = .white
  }
  struct EnterpriseImageView {
    struct Sizes {
      static let lateralsMargin: CGFloat = 16
      static let topMargin: CGFloat = 16
      static let height: CGFloat = 200
      static let borderValue: CGFloat = 8.0
    }
  }
  struct DescriptionTextView {
    struct Fonts {
      static let description = UIFont.systemFont(ofSize: 20, weight: .medium)
    }
    struct Colors {
      static let textColor: UIColor = .black
    }
    struct Sizes {
      static let generalMargin: CGFloat = 16
    }
  }
}
