//
//  EnterpriseStyleKit.swift
//  Enterprise
//
//  Created by Robson James Junior on 22/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit

struct EnterpriseStyleKit {
  struct MainScreen {
    struct Colors {
      static let backGroundColor = UIColor(red: 235, green: 233, blue: 215)
    }
    struct Sizes {
      static let heightForRowAt: CGFloat = 120
    }
  }
  
  struct SearchApparence {
    struct Texts {
      static let placeHolderText: String = "Buscar"
    }
    struct Colors {
      static let tintColor: UIColor = .white
      static let barBackgroundColor: UIColor = .white
    }
  }
  struct NavigationApparence {
    struct Texts {
      static let logoName: String = "logo_nav"
    }
    struct Colors {
      static let backgroundColor = UIColor(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    }
    struct Sizes {
      static let heightBanner: CGFloat = 30
      static let widthBanner: CGFloat = 120
    }
  }
}

struct EnterprisesCellStyleKit {
  struct ContentView {
    struct Sizes {
      static let spacingInContainer: CGFloat = 8
    }
    struct Colors {
      static let backgroundColor: UIColor = UIColor(red: 255, green: 255, blue: 255)
    }
  }
  struct EnterprisesImage {
    struct Sizes {
      static let heightView: CGFloat = 80
      static let widthView: CGFloat = 105
      static let distanceToLeft: CGFloat = 8
    }
  }
  struct StackViewLabel {
    struct EnterprisesName {
      static let font = UIFont.systemFont(ofSize: 20, weight: .bold)
    }
    struct EnterprisesTypeName {
      static let font = UIFont.systemFont(ofSize: 18, weight: .ultraLight)
    }
    struct EnterprisesCountry {
      static let font = UIFont.systemFont(ofSize: 18, weight: .ultraLight)
    }
    struct Sizes {
      static let distanceToLeft: CGFloat = 16.0
      static let spacingBettwenLabels: CGFloat = 4.0
    }
  }
}
