//
//  StyleKit.swift
//  Enterprise
//
//  Created by Robson James Junior on 21/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import Foundation
import UIKit

struct LoginSK {
  struct StackViewLabels {
    struct Labels {
      struct TitleProperty {
        static let text: String = "BEM-VINDO AO EMPRESAS"
        static let font = UIFont.systemFont(ofSize: 26, weight: .bold)
      }
      struct SubTitleProperty {
        static let text: String = "Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan."
        static let font = UIFont.systemFont(ofSize: 14, weight: .regular)
      }
    }
  }
  
  struct LoginButton {
    struct Colors {
      static let backGroundColor = UIColor(red: 42, green: 139, blue: 175)
      static let textColor = UIColor(red: 235, green: 250, blue: 246)
    }
    struct Sizes {
      static let margin: CGFloat = 50
      static let cornerRadiusValue: CGFloat = 26
      static let heightValue: CGFloat = 52
      static let spacingToTop: CGFloat = 56
    }
    struct Texts {
      static let title: String = "Entrar"
    }
    struct Font {
      static let title = UIFont.systemFont(ofSize: 24, weight: .semibold)
    }
  }
  
  struct EmailTextField {
    struct Colors {
      static let subtitles = UIColor(red: 93, green: 90, blue: 90)
      static let borderColor = UIColor(red: 151, green: 151, blue: 151).cgColor
      static let textColor = UIColor(red: 235, green: 250, blue: 246)
    }
    struct Sizes {
      static let margin: CGFloat = 32
      static let heightValue: CGFloat = 45
      static let cornerRadiusValue: CGFloat = 10
      static let borderWidthValue: CGFloat = 1
      static let marginToTop: CGFloat = 35
    }
    struct Texts {
      static let iconName: String = "ic_email"
      static let placeHolder: String = "e-mail"
    }
    struct Font {
      
    }
  }
  
  struct PasswordTextField {
    struct Colors {
      static let subtitles = UIColor(red: 93, green: 90, blue: 90)
      static let borderColor = UIColor(red: 151, green: 151, blue: 151).cgColor
      static let textColor = UIColor(red: 235, green: 250, blue: 246)
    }
    struct Sizes {
      static let margin: CGFloat = 32
      static let heightValue: CGFloat = 45
      static let cornerRadiusValue: CGFloat = 10
      static let borderWidthValue: CGFloat = 1
      static let marginToBottom: CGFloat = 25
    }
    struct Texts {
      static let iconName: String = "ic_cadeado"
      static let placeHolder: String = "senha"
    }
    struct Font {
      
    }
  }
  
  struct LogoBanner {
    struct Sizes {
      static let widthValue: CGFloat = 185
      static let heightValue: CGFloat = 45
      static let marginToTop: CGFloat = 62
    }
    struct Texts {
      static let imageName: String = "logo_home"
    }
  }
  
  struct MainScreen {
    struct Colors {
      static let backGroundColor = UIColor(red: 235, green: 233, blue: 215)
    }
  }
}
