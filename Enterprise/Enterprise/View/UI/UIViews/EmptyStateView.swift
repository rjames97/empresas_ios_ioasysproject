//
//  EmptyStateView.swift
//  Enterprise
//
//  Created by Robson James Junior on 23/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import Foundation
import UIKit
import Stevia

struct EmptyViewStyle {
  struct Fonts {
    static let emptyFont = UIFont.systemFont(ofSize: 22, weight: .bold)
  }
  struct Texts {
    static let emptyText: String = "Nehuma Empresa foi encontrada para a busca realizada"
  }
  struct Sizes {
    static let margins: CGFloat = 16
    static let heigth: CGFloat = 400
  }
}

class EmptyStateView: UIView, ArchitectureStandard {
  
  private let labelDescription = UILabel()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  func style() {
    self.backgroundColor = .clear
    labelDescription.text = EmptyViewStyle.Texts.emptyText
    labelDescription.font = EmptyViewStyle.Fonts.emptyFont
    labelDescription.numberOfLines = 0
    labelDescription.textAlignment = .center
  }
  
  func subview() {
    self.sv(labelDescription)
  }
  
  func layout() {
    labelDescription.Top == safeAreaLayoutGuide.Top + EmptyViewStyle.Sizes.margins
    labelDescription.fillHorizontally(m: EmptyViewStyle.Sizes.margins)
  }
}

