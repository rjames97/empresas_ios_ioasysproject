//
//  SpinnerViewController.swift
//  Enterprise
//
//  Created by Robson James Junior on 21/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import Foundation
import UIKit

class SpinnerViewController: UIViewController {
  var spinner = UIActivityIndicatorView(style: .large)
  private static var privateShared: SpinnerViewController?
  class var shared: SpinnerViewController {
    guard let uwShared = privateShared else {
      privateShared = SpinnerViewController()
      return privateShared!
    }
    return uwShared
  }
  @objc class func destroy() {
    privateShared = nil
  }
  override func loadView() {
    view = UIView()
    view.backgroundColor = UIColor(white: 0, alpha: 0.7)
    spinner.translatesAutoresizingMaskIntoConstraints = true
    spinner.startAnimating()
    spinner.color = UIColor(red: 0.8479598165, green: 0.2563658357, blue: 0.4312595129, alpha: 1)
    view.sv(spinner)
    spinner.centerVertically()
    spinner.centerHorizontally()
  }
}

extension SpinnerViewController {
  public func createSpinnerView(vController: UIViewController) {
    vController.addChild(self)
    self.view.frame = vController.view.frame
    vController.view.addSubview(self.view)
    self.view.fillContainer()
    self.didMove(toParent: self)
  }
  
  public func destroySpinner() {
    DispatchQueue.main.async {
      self.willMove(toParent: nil)
      self.view.removeFromSuperview()
      self.removeFromParent()
    }
  }
}
