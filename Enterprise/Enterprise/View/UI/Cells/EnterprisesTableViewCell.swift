//
//  EnterprisesTableViewCell.swift
//  Enterprise
//
//  Created by Robson James Junior on 22/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit
import Stevia

class EnterprisesTableViewCell: UITableViewCell, ArchitectureStandard {
  
  typealias contentViewStyle = EnterprisesCellStyleKit.ContentView
  typealias imageViewStyle = EnterprisesCellStyleKit.EnterprisesImage
  typealias stackLabelsStyle = EnterprisesCellStyleKit.StackViewLabel
  
  private var viewForCell = UIView()
  public let enterpriseImageView = UIImageView()
  private let labelsStackView = UIStackView()
  public let enterpriseNameLabel = UILabel()
  public let enterpriseTypeNameLabel = UILabel()
  public let enterpriseCountryLabel = UILabel()
  
  public static let identifier = "EnterprisesTableViewCell"
  
  override func awakeFromNib() {
    super.awakeFromNib()
    commonInit()
  }
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: EnterprisesTableViewCell.identifier)
    commonInit()
  }
  
  func style() {
    backgroundColor = .clear
    enterpriseImageView.backgroundColor = .green //TODO: The API does not work with the image, it changes when the API is updated
    viewForCell.backgroundColor = contentViewStyle.Colors.backgroundColor
    viewForCell.cropView(withValue: 8)
    
    enterpriseImageView.cropView(withValue: 8)
    enterpriseNameLabel.font = stackLabelsStyle.EnterprisesName.font
    enterpriseTypeNameLabel.font = stackLabelsStyle.EnterprisesTypeName.font
    enterpriseCountryLabel.font = stackLabelsStyle.EnterprisesCountry.font
  }
  
  func subview() {
    self.sv(viewForCell)
    viewForCell.sv(enterpriseImageView)
    labelsStackView.addArrangedSubview(enterpriseNameLabel)
    labelsStackView.addArrangedSubview(enterpriseTypeNameLabel)
    labelsStackView.addArrangedSubview(enterpriseCountryLabel)
    viewForCell.sv(labelsStackView)
  }
  
  func layout() {
    viewForCell.fillContainer(contentViewStyle.Sizes.spacingInContainer)
    
    enterpriseImageView.height(imageViewStyle.Sizes.heightView)
    enterpriseImageView.width(imageViewStyle.Sizes.widthView)
    enterpriseImageView.left(imageViewStyle.Sizes.distanceToLeft)
    enterpriseImageView.centerVertically()
    
    labelsStackView.centerVertically()
    labelsStackView.Left == enterpriseImageView.Right + stackLabelsStyle.Sizes.distanceToLeft
    labelsStackView.axis = .vertical
    labelsStackView.alignment = .leading
    labelsStackView.spacing = stackLabelsStyle.Sizes.spacingBettwenLabels
  }
  
  override func prepareForReuse() {
    self.enterpriseImageView.image = nil
    self.enterpriseNameLabel.text = nil
    self.enterpriseTypeNameLabel.text = nil
    self.enterpriseCountryLabel.text = nil
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
