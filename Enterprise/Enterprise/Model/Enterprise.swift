//
//  Enterprise.swift
//  Enterprise
//
//  Created by Robson James Junior on 23/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit

class EnterpriseGroup: Decodable {
  public let enterprises: [Enterprise]
  
  enum CodingKeys: String, CodingKey {
    case enterprises
  }
  
  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    enterprises = try container.decode([Enterprise].self, forKey: .enterprises)
  }
}

class Enterprise: Decodable {
  
  public let name: String
  public let imageUrl: String?
  public let description: String
  public let city: String
  public let country: String
  public let enterpriseType: EnterpriseType
  
  enum CodingKeys: String, CodingKey {
    case city, country, photo, enterprise_name, enterprise_type, description
  }
  
  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    name = try container.decode(String.self, forKey: .enterprise_name)
    description = try container.decode(String.self, forKey: .description)
    city = try container.decode(String.self, forKey: .city)
    country = try container.decode(String.self, forKey: .country)
    imageUrl = try container.decodeIfPresent(String.self, forKey: .photo)
    enterpriseType = try container.decode(EnterpriseType.self,
                                          forKey: .enterprise_type)
  }
}

class EnterpriseType: Decodable {
  public let category: String
  
  enum CodingKeys: String, CodingKey {
    case enterprise_type_name
  }
  
  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    category = try container.decode(String.self, forKey: .enterprise_type_name)
  }
}
