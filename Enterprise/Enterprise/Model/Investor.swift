//
//  Investor.swift
//  Enterprise
//
//  Created by Robson James Junior on 22/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import Foundation

import Foundation

class Portifolio: Decodable {
  public var enterpriseNumber: Int
  public var enterprises: [String]
  enum CodingKeys: String, CodingKey {
    case enterprises_number, enterprises
  }
  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    enterpriseNumber = try container.decode(Int.self, forKey: .enterprises_number)
    enterprises = try container.decode([String].self, forKey: .enterprises)
  }
}

class Investor: Decodable {
  public let name: String
  public let email: String
  public let city: String
  public let country: String
  public let imageUrl: String
  public let portifolio: Portifolio
  public let firstAcess: Bool
  public let superAngel: Bool
  
  enum CodingKeys: String, CodingKey {
    case email, city, country, photo, first_access, super_angel, investor_name, portfolio
  }
  
  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    name = try container.decode(String.self, forKey: .investor_name)
    email = try container.decode(String.self, forKey: .email)
    city = try container.decode(String.self, forKey: .city)
    country = try container.decode(String.self, forKey: .country)
    imageUrl = try container.decode(String.self, forKey: .photo)
    portifolio = try container.decode(Portifolio.self, forKey: .portfolio)
    firstAcess = try container.decode(Bool.self, forKey: .first_access)
    superAngel = try container.decode(Bool.self, forKey: .super_angel)
  }
}
