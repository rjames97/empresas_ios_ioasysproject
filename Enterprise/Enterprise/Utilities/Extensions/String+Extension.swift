//
//  String+Extension.swift
//  Enterprise
//
//  Created by Robson James Junior on 21/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import Foundation

extension String {
  public static let oAuthKey = "OAuth2"
  
  public func numberOfOccurrences(_ string: String) -> Int {
    return components(separatedBy: string).count - 1
  }
  
  func isValidEmail() -> Bool {
    let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
    return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
  }
}
