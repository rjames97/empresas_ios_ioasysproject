//
//  Notification+Extension.swift
//  Enterprise
//
//  Created by Robson James Junior on 22/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import Foundation

extension Notification.Name {
  static let signSucess = Notification.Name("signSucess")
  static let signFail = Notification.Name("signFail")
  static let enterprisesSucess = Notification.Name("enterprisesSucess")
  static let enterprisesFail = Notification.Name("enterprisesFail")
  static let searchedEnterprises = Notification.Name("searchedEnterprises")
}
