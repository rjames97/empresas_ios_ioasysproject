//
//  UIView+Extension.swift
//  Enterprise
//
//  Created by Robson James Junior on 23/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit

extension UIView {
  public func cropView(withValue bordeValue: CGFloat) {
    self.contentMode = .scaleAspectFill
    self.layer.cornerRadius = bordeValue
    self.clipsToBounds = true
    self.layer.masksToBounds = true
  }
}
