//
//  UITextField+Extension.swift
//  Enterprise
//
//  Created by Robson James Junior on 21/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit
import Stevia

extension UITextField {
  func setIcon(_ image: UIImage, height: CGFloat, width: CGFloat) {
    let iconView = UIImageView(frame:
      CGRect(x: 10, y: 10, width: height, height: width))
    iconView.image = image
    let iconContainerView: UIView = UIView(frame:
      CGRect(x: 20, y: 0, width: 40, height: 40))
    iconContainerView.addSubview(iconView)
    iconView.centerVertically().centerHorizontally()
    leftView = iconContainerView
    leftViewMode = .always
  }
  
  func setSystemIcon(_ imageName: String) {
    guard let image = UIImage(systemName: imageName) else { return
    }
    let iconView = UIImageView(frame:
      CGRect(x: 10, y: 10, width: 20, height: 20))
    iconView.image = image
    let iconContainerView: UIView = UIView(frame:
      CGRect(x: 20, y: 0, width: 40, height: 40))
    iconContainerView.addSubview(iconView)
    leftView = iconContainerView
    leftViewMode = .always
    tintColor =  UIColor.lightGray
  }
}
