//
//  UIViewController+Extension.swift
//  Enterprise
//
//  Created by Robson James Junior on 21/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
  
  public func getImage(imageName:String) -> UIImage? {
    guard let image = UIImage(named: imageName) else {
      return nil
    }
    return image
  }
  
  public func showAlert(title: String,
                        message: String,
                        buttonTitle: String,
                        style: UIAlertController.Style = .alert,
                        buttonHandle: ((UIAlertAction) -> Void)?,
                        buttonStyle: UIAlertAction.Style = .default,
                        cancelButtonTitle: String? = nil,
                        cancelButtonHandle: ((UIAlertAction) -> Void)? = nil,
                        cancelStyle: UIAlertAction.Style = .cancel,
                        completion:(() -> Void)?) {
    
    let alert = UIAlertController(title: title,
                                  message: message,
                                  preferredStyle: style)
    
    alert.addAction(UIAlertAction(title: buttonTitle,
                                  style: buttonStyle,
                                  handler: buttonHandle))
    if cancelButtonTitle != nil {
      alert.addAction(UIAlertAction(title: cancelButtonTitle,
                                    style: cancelStyle,
                                    handler: cancelButtonHandle))
    }
    self.present(alert, animated: true, completion: completion)
  }
}
