//
//  CloudManager.swift
//  Enterprise
//
//  Created by Robson James Junior on 22/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import Foundation

class CloudManager {
  private static var privateShared: CloudManager?
  private var auth: OAuth? {
    didSet { UserDefaults.standard.setStruct(auth, forKey: .oAuthKey) }
  }
  
  class var shared: CloudManager {
    guard let uwShared = privateShared else {
      privateShared = CloudManager()
      return privateShared!
    }
    return uwShared
  }
  
  class func destroy() {
    privateShared?.auth = nil
    privateShared = nil
  }
  
  private init() {
    if let auth = UserDefaults.standard.structData(OAuth.self, forKey: .oAuthKey) {
      self.auth = auth
    } else {
      self.auth = nil
    }
  }
  
  public func sign(user: User) {
    CloudUtils.signIn(user: user) { (client, auth, err) in
      if client != nil, auth != nil {
        NotificationCenter.default.post(name: .signSucess, object: nil)
        self.auth = auth
      } else if err != nil {
        NotificationCenter.default.post(name: .signFail,
                                        object: nil)
      }
    }
  }
  
  public func showAllEnterprises() {
    CloudUtils.getAllEnterprises { (enterprises, erroObject, error) in
      if let enterprises = enterprises, !enterprises.isEmpty {
        let userInfoData:[String: [Enterprise]] = ["enterprises": enterprises]
        NotificationCenter.default.post(name: .enterprisesSucess,
                                        object: nil,
                                        userInfo: userInfoData)
      } else {
        if let error = erroObject {
          var userInfoData:[String: [String]]? = nil
          if let message = error.errorString {
            userInfoData = ["errorMessage": message]
          }
          NotificationCenter.default.post(name: .enterprisesFail,
                                          object: nil,
                                          userInfo: userInfoData)
        }
      }
    }
  }
  
  public func searchEnterprise(with name: String) {
    CloudUtils.searchEnterprise(by: name) { (enterprises, erroObject, error) in
      if let enterprises = enterprises {
        let userInfoData:[String: [Enterprise]] = ["searchedEnterprises": enterprises]
        NotificationCenter.default.post(name: .searchedEnterprises,
                                        object: nil,
                                        userInfo: userInfoData)
      } else {
        if let error = erroObject {
          var userInfoData:[String: [String]]? = nil
          if let message = error.errorString {
            userInfoData = ["errorMessage": message]
          }
          NotificationCenter.default.post(name: .enterprisesFail,
                                          object: nil,
                                          userInfo: userInfoData)
        }
      }
    }
  }
}
