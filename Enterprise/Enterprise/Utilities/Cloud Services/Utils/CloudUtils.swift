//
//  CloudUtils.swift
//  Enterprise
//
//  Created by Robson James Junior on 22/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import Foundation
import UIKit

class CloudUtils {
  
  private let cloudManager = CloudManager.shared
  public typealias oAuthHeader = OAuth.Headers
  
  private static let limitTimeResource: TimeInterval = 60
  private static let limitTimeRequest: TimeInterval = 30
  
  static let baseUrl: String = "https://empresas.ioasys.com.br/api/v1"
  
  internal enum OperationClient: String {
    case sign
    case enterprisesShow
    case searchEnterprise
    
    var url: String {
      switch self {
        case .sign:
          return "\(CloudUtils.baseUrl)/users/auth/sign_in"
        case .enterprisesShow:
          return "\(CloudUtils.baseUrl)/enterprises"
        case .searchEnterprise:
          return "\(CloudUtils.baseUrl)/enterprises?name=%@"
      }
    }
  }
  
  internal enum HttpHeader: String {
    case contentType = "Content-Type"
    case applicationJson = "Application/json"
  }
  
  internal enum HttpMethods: String {
    case get = "GET"
    case post = "POST"
  }
  
  static public func postRequest(url: URL, body: Data?, headersForAuth: OAuth? = nil, completion: ((_ data: Data?,_ reponse: URLResponse? ,_ error: Error?) -> Void)?) {
    request(url: url, method: .post, body: body, completion: completion)
  }
  
  static public func getRequest(url: URL, body: Data?, headersForAuth: OAuth? = nil, completion: ((_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void)?) {
    request(url: url, method: .get, body: body, headersForAuth: headersForAuth, completion: completion)
  }
  
  private static func request(url: URL, method: HttpMethods, body: Data?,
                              headersForAuth: OAuth? = nil,
                              completion: ((_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void)?) {
    var request = URLRequest(url: url)
    request.httpMethod = method.rawValue
    request.addValue(HttpHeader.applicationJson.rawValue,
                     forHTTPHeaderField: HttpHeader.contentType.rawValue)
    
    if let headers = headersForAuth {
      request.addValue(headers.accessToken,
                       forHTTPHeaderField: oAuthHeader.accessToken.rawValue)
      request.addValue(headers.client,
                       forHTTPHeaderField: oAuthHeader.client.rawValue)
      request.addValue(headers.uid,
                       forHTTPHeaderField: oAuthHeader.uid.rawValue)
    }
    if let data = body {
      request.httpBody = data
    }
    let configuration = URLSessionConfiguration.default
    configuration.timeoutIntervalForRequest = limitTimeRequest
    configuration.timeoutIntervalForResource = limitTimeResource
    let session = URLSession.init(configuration: configuration)
    session.dataTask(with: request) { (data, response, error) in
      if let data = data, let response = response {
        completion?(data, response, nil)
      } else if let error = error, let response = response {
        completion?(nil, response, error)
      } else if let error = error {
        print("Error: \(error), line: \(#line), func: \(#function)")
        completion?(nil, nil, error)
      }
    }.resume()
  }
  
  public static func signIn(user: User, completion: ((_ client: Client?,_ oauth: OAuth?,_ error: Error?) -> Void)?) {
    guard let url = URL(string: OperationClient.sign.url),
      let dataUser = try? JSONEncoder().encode(user) else {
        return
    }
    self.postRequest(url: url, body: dataUser) { (data, response, error) in
      if let data = data, let response = response as? HTTPURLResponse {
        do {
          let client = try JSONDecoder().decode(Client.self, from: data)
          var auth: OAuth?
          if let accessToken = response.allHeaderFields[oAuthHeader.accessToken.rawValue] as? String,
            let client = response.allHeaderFields[oAuthHeader.client.rawValue] as? String,
            let uid = response.allHeaderFields[oAuthHeader.uid.rawValue] as? String {
            auth = OAuth(accessToken: accessToken, client: client, uid: uid)
          }
          completion?(client, auth, nil)
        } catch let errorJSON {
          print("JSON Error: \(errorJSON), line: \(#line), func: \(#function)")
          completion?(nil, nil, errorJSON)
          NotificationCenter.default.post(name: .enterprisesFail,
                                          object: nil,
                                          userInfo: nil)
        }
      } else if error != nil {
        completion?(nil, nil,error)
      }
    }
  }
  
  public static func getAllEnterprises(completion: ((_ enterprises: [Enterprise]?,_ errorCloud: ErrorsCloud?, _ error: Error?) -> Void)?) {
    guard let auth = UserDefaults().structData(OAuth.self, forKey: .oAuthKey) else {
      return
    }
    guard let url = URL(string: OperationClient.enterprisesShow.url) else {
      return
    }
    self.getRequest(url: url, body: nil, headersForAuth: auth) { (data, _, error) in
      if let data = data {
        do {
          let enterpriseGroup = try JSONDecoder().decode(EnterpriseGroup.self, from: data)
          completion?(enterpriseGroup.enterprises, nil, nil)
        } catch {
          do {
            let errorObject = try JSONDecoder().decode(ErrorsCloud.self, from: data)
            completion?(nil, errorObject, nil)
          } catch let errorJSON {
            print("JSON Error: \(errorJSON), line: \(#line), func: \(#function)")
            completion?(nil, nil, error)
          }
        }
      } else if let err = error {
        print("Error: \(err), line: \(#line), func: \(#function)")
        NotificationCenter.default.post(name: .enterprisesFail,
                                        object: nil,
                                        userInfo: nil)
      }
    }
  }
  
  public static func searchEnterprise(by name: String, completion: ((_ enterprise: [Enterprise]?,_ errorCloud: ErrorsCloud?, _ error: Error?) -> Void)?) {
    guard let auth = UserDefaults().structData(OAuth.self, forKey: .oAuthKey) else {
      return
    }
    let urlString = String(format: OperationClient.searchEnterprise.url, name)
    guard let url = URL(string: urlString) else {
      return
    }
    getRequest(url: url,
               body: nil,
               headersForAuth: auth) { (data, _, error) in
                if let data = data {
                  do {
                    let enterpriseGroup = try JSONDecoder().decode(EnterpriseGroup.self, from: data)
                    completion?(enterpriseGroup.enterprises, nil, nil)
                  } catch {
                    do {
                      let errorObject = try JSONDecoder().decode(ErrorsCloud.self, from: data)
                      completion?(nil, errorObject, nil)
                    } catch let errorJSON {
                      print("JSON Error: \(errorJSON), line: \(#line), func: \(#function)")
                      completion?(nil, nil, error)
                    }
                  }
                } else if let err = error {
                  print("Error: \(err), line: \(#line), func: \(#function)")
                  NotificationCenter.default.post(name: .enterprisesFail,
                                                  object: nil,
                                                  userInfo: nil)
                }
    }
  }
}
