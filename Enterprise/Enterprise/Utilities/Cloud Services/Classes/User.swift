//
//  User.swift
//  Enterprise
//
//  Created by Robson James Junior on 22/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import Foundation

struct User: Codable {
    var email:String
    var password:String
}
