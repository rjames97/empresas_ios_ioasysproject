//
//  OAuth2.swift
//  Enterprise
//
//  Created by Robson James Junior on 22/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import Foundation

struct OAuth: Codable {
  public var accessToken: String
  public var client: String
  public var uid: String
  
  public enum Headers: String {
    case accessToken = "access-token", client, uid
  }
  
  enum CodingKeys: String, CodingKey {
    case accessToken = "access-token", client, uid
  }
}
