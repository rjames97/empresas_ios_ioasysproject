//
//  Client.swift
//  Enterprise
//
//  Created by Robson James Junior on 22/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import Foundation

class Client: Decodable {
  public let investor: Investor
  enum CodingKeys: String, CodingKey {
    case investor
  }
  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    investor = try container.decode(Investor.self, forKey: .investor)
  }
}
