//
//  Error.swift
//  Enterprise
//
//  Created by Robson James Junior on 23/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import UIKit

class ErrorsCloud: Decodable {
  public let errorString: [String]?
  
  enum CodingKeys: String, CodingKey {
    case errors
  }
  
  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    errorString = try container.decode([String].self, forKey: .errors)
  }
}
