//
//  File.swift
//  Enterprise
//
//  Created by Robson James Junior on 21/02/20.
//  Copyright © 2020 robsonJames. All rights reserved.
//

import Foundation

protocol ArchitectureStandard {
  func style()
  func subview()
  func layout()
}

extension ArchitectureStandard {
  func commonInit() {
    subview()
    layout()
    style()
  }
}
